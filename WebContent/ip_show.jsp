<%@page import="com.shieldsquare.ip2loc.model.IP2loc_model"%>
<%@page language="java" import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>

<body>
<h2>Details of IPs</h2>
<table align="center" border="5" style="width: 100%;">
	<tbody>
	</tbody>
	<colgroup>
		<col width="90/" />
		<col width="90" />
		<col width="80" />
		<col width="80" />
		<col width="80" />
		<col width="80" />
		<col width="80" />
	</colgroup>
	<tbody>
		<tr>
			<th style="text-align: center; background-color: yellow"><span style="font-size:14px"><span style="color:#000000"><span style="font-family:georgia,times,times new roman,serif"><strong>IP Address</strong></span></span></span></th>
			<th style="text-align: center; background-color: yellow"><span style="font-size:14px"><span style="color:#000000"><strong><span style="font-family:georgia,times,times new roman,serif">ISP</span></strong></span></span></th>
			 <th style="text-align: center; background-color: yellow"><span style="font-size:14px"><span style="color:#000000"><span style="font-family:georgia,times,times new roman,serif"><strong>City</strong></span></span></span></th>
			<th style="text-align: center; background-color: yellow"><span style="font-size:14px"><span style="color:#000000"><span style="font-family:georgia,times,times new roman,serif"><strong>Country</strong></span></span></span></th>
			<th style="text-align: center; background-color: yellow"><span style="font-size:14px"><span style="color:#000000"><span style="font-family:georgia,times,times new roman,serif"><strong>Domain Name</strong></span></span></span></th>
			<th style="text-align: center; background-color: yellow"><span style="font-size:14px"><span style="color:#000000"><span style="font-family:georgia,times,times new roman,serif"><strong>IP_Type</strong></span></span></span></th>
			<th style="text-align: center; background-color: yellow"><span style="font-size:14px"><span style="color:#000000"><span style="font-family:georgia,times,times new roman,serif"><strong>NSLookup</strong></span></span></span></th>	
	 
		</tr>
		

			<% ArrayList<IP2loc_model> ipDList= (ArrayList<IP2loc_model>)request.getAttribute("ipdetails");
	String dch="DCH";
	for (IP2loc_model ipD : ipDList) {
	%>

		<tr>
			<td style="text-align: center; background-color: #ECF0F1"><span style="font-size:13px"><span style="color:#000000"><span style="font-family:times new roman,times,baskerville,georgia,serif"><%=ipD.getIp_address()%></span></span></span></td>
			<td style="text-align: center; background-color: #FDEDEC"><span style="font-family:georgia,times,times new roman,serif"><span style="font-size:12px"><span style="color:rgb(0, 0, 0)"><%=ipD.getIsp()%></span></span></span></td>			
		 	<td style="text-align: center; background-color: #EBF5FB"><span style="font-family:georgia,times,times new roman,serif"><span style="font-size:12px"><span style="color:rgb(0, 0, 0)"><%=ipD.getCity()%></span></span></span></td>
			<td style="text-align: center; background-color: #F8F9F9"><span style="font-size:13px"><span style="color:#000000"><span style="font-family:times new roman,times,baskerville,georgia,serif"><%=ipD.getCountry()%></span></span></span></td>
			<td style="text-align: center; background-color: #E8F8F5"><span style="font-size:13px"><span style="color:#000000"><span style="font-family:times new roman,times,baskerville,georgia,serif"><%=ipD.getDomain()%></span></span></span></td>
			<td style="text-align: center; background-color: #F9EBEA"><span style="font-size:13px"><span style="color:#000000"><span style="font-family:times new roman,times,baskerville,georgia,serif"><%=ipD.getIp_type()%></span></span></span></td>
			<td style="text-align: center; background-color: #F9EBEA"><span style="font-size:13px"><span style="color:#000000"><span style="font-family:times new roman,times,baskerville,georgia,serif"><%=ipD.getNslookup()%></span></span></span></td>			
		 			
				<%}%> 
	</table>



</body>
</html>