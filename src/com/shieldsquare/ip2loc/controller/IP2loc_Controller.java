package com.shieldsquare.ip2loc.controller;

import java.awt.SystemColor;
import java.io.IOException;
import java.util.ArrayList;

import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shieldsquare.ip2loc.model.IP2loc_model;
import com.shieldsquare.ip2loc.services.IP2loc_service;

/**
 * @author yogesh 
 * Servlet implementation class IP2loc_Controller
 */
public class IP2loc_Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String s = request.getParameter("ip");
		String[] ss = s.trim().split(",");
		IP2loc_service ip2l = new IP2loc_service();
		ArrayList<IP2loc_model> ipdetails;
		
		try {
			ipdetails = ip2l.getIP2location(ss);
			RequestDispatcher rd = request.getRequestDispatcher("ip_show.jsp");
			request.setAttribute("ipdetails", ipdetails);
			rd.forward(request, response);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	
	}
}
