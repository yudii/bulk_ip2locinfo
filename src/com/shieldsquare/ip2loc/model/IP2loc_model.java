package com.shieldsquare.ip2loc.model;

public class IP2loc_model {

	private String ip_address;
	private String city;
	private String country;
	private String isp;
	private String domain;
	private String ip_type;
	
	private String nslookup;
	
	
	
	
	public String getNslookup() {
		return nslookup;
	}
	public void setNslookup(String nslookup) {
		this.nslookup = nslookup;
	}
	public String getIp_address() {
		return ip_address;
	}
	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
	
		this.country = country;
		
	}
	public String getIsp() {
		return isp;
	}
	public void setIsp(String isp) {
		this.isp = isp;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getIp_type() {
		return ip_type;
	}
	public void setIp_type(String ip_type) {
		
		this.ip_type = ip_type;
	}
	
	
}
