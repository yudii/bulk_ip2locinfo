package com.shieldsquare.ip2loc.services;

import java.io.IOException;
import java.util.ArrayList;

import javax.naming.NamingException;

import sun.security.util.Length;

import com.ip2location.IP2Location;
import com.ip2location.IPResult;
import com.shieldsquare.ip2loc.model.IP2loc_model;

/**
 * @author yogesh
 * 
 **/

public class IP2loc_service {

	public ArrayList<IP2loc_model> getIP2location(String[] ss) throws IOException, NamingException {
		
		String isp = null;
		String city = null;
		String country = null;
		String domain = null;
		String ip_type = null;
		ArrayList<IP2loc_model> ip2Loc = new ArrayList<IP2loc_model>();
		
		IP2Location loc = new IP2Location();
	//	IP2Location.IPDatabasePath = "/home/yogesh/IP2LOC/IP-COUNTRY-REGION-CITY-LATITUDE-LONGITUDE-ISP-DOMAIN-MOBILE-USAGETYPE.BIN";
	//	String licensePath = "/home/yogesh/IP2LOC/license.key";
	
		IP2Location.IPDatabasePath = "/usr/local/tomcat6/webapps/IPV6-COUNTRY-REGION-CITY-LATITUDE-LONGITUDE-ISP-DOMAIN-MOBILE-USAGETYPE.BIN";
		String licensePath = "/usr/local/tomcat6/webapps/license.key";

		if (licensePath != null && !licensePath.isEmpty())
			IP2Location.IPLicensePath = licensePath;
		IPResult rec = null;
		
		
		
		for (int i = 0; i <= ss.length - 1; i++) {
			String ipp = ss[i];
		
			if (ipp.trim().length() > 15) {
				ipp = "Entered IP is not proper. Ur given IP is " + ipp;
			
				/*String ipp2=ipp.split(".");
				*/
			
			}
			IP2loc_model ipd = new IP2loc_model();
			
			try {
				rec = loc.IPQuery(ipp);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (rec != null) {
				String ip_address = ipp;
				isp = rec.getISP();
				city = rec.getCity();
				country = rec.getCountryLong();
				domain = rec.getDomain();
				ip_type = rec.getUsageType();

				ipd.setIp_address(ip_address);
				ipd.setIsp(isp);
				ipd.setCity(city);
				ipd.setCountry(country);
				ipd.setDomain(domain);
				ipd.setIp_type(ip_type);
				
				String nslookup1=nslookup(ip_address);
				
				ipd.setNslookup(nslookup1);
				ip2Loc.add(ipd);
			}
		}
		return ip2Loc;
	}
	
	
	//NSLookup
	
	
	
	public static String nslookup(String args) throws IOException,NamingException {
		String retVal = null;
		String ip = args;
		if(ip == null)
			return retVal;
		final String[] bytes = ip.split("\\.");
		if (bytes.length == 4) {
			try {
				final java.util.Hashtable<String, String> env = new java.util.Hashtable<String, String>();
				env.put("java.naming.factory.initial","com.sun.jndi.dns.DnsContextFactory");
				final javax.naming.directory.DirContext ctx = new javax.naming.directory.InitialDirContext(env);
				final String reverseDnsDomain = bytes[3] + "." + bytes[2] + "."+ bytes[1] + "." + bytes[0] + ".in-addr.arpa";
				final javax.naming.directory.Attributes attrs = ctx
				.getAttributes(reverseDnsDomain,
						new String[] { "PTR", });
	
				for (final javax.naming.NamingEnumeration<? extends javax.naming.directory.Attribute> ae = attrs
				.getAll(); ae.hasMoreElements();) {
			
					final javax.naming.directory.Attribute attr = ae.next();
					final String attrId = attr.getID();			
					
					for (final java.util.Enumeration<?> vals = attr.getAll(); vals
					.hasMoreElements();) {
				String value = vals.nextElement().toString();
				if ("PTR".equals(attrId)) {
					final int len = value.length();
					if (value.charAt(len - 1) == '.') {
						// Strip out trailing period
						value = value.substring(0, len - 1);
					}
					retVal = value;
				}
			}
		}
		ctx.close();
	} catch (final javax.naming.NamingException e) {
		// No reverse DNS that we could find, try with InetAddress
		retVal = "No reverse DNS found"; // NO-OP
	}
}
		if (null == retVal) {
			try {
				retVal = java.net.InetAddress.getByName(ip)
				.getCanonicalHostName();
			} catch (final java.net.UnknownHostException e1) {
				retVal = ip;
			}
		}
		return retVal;
	}
	
	
	
	
	
	
	
	
	
}
